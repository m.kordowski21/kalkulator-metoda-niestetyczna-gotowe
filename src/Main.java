import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

       Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz wpisać: ");
        int liczby = scanner.nextInt();

        int[] array = new int[liczby];

        Kalkulator kalkulator = new Kalkulator(array);

        for ( int i = 0; i < array.length; i++) {
            System.out.println("Podaj liczbę:" );
            int podanaLiczba = scanner.nextInt();
            scanner.nextLine();
            array[i] = podanaLiczba;


        }


        System.out.println("Podaj jakie działanie chcesz wykonać?: dodawanie, odejmowanie, mnozenie, dzielenie");
        String dzialanie = scanner.nextLine();



        switch (dzialanie){
            case "dodawanie":
                System.out.println("Suma liczb = " + kalkulator.getSuma());
                break;
            case "odejmowanie":
                System.out.println("Roznica liczb = "+kalkulator.getRoznica());
                break;
            case "mnozenie":
                System.out.println("Mnożenie liczb = " + kalkulator.getMnozenie() );
                break;
            case "dzielenie":
                System.out.println("Dzielenie liczb = " + kalkulator.getDzielenie() );
                break;
            default:
                System.out.println("Błędnie wpisałeś działanie!!");
        }







    }
}
